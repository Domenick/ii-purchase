﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется посредством следующего 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("IiPurchase.Tests.StorageComponent")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("IiPurchase.Tests.StorageComponent")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Задание значения false для атрибута ComVisible приведет к тому, что типы из этой сборки станут невидимыми 
// для COM-компонентов.  Если к одному из типов этой сборки необходимо обращаться из 
// модели COM, задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Если данный проект доступен для модели COM, следующий GUID используется в качестве идентификатора библиотеки типов
[assembly: Guid("763d71e7-107e-486d-9022-c25f5241ad90")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// [сборка: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
