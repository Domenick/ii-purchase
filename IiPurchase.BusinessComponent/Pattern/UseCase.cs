﻿namespace IiPurchase.BusinessComponent.Pattern
{
    public interface UseCase<TRequestModel> where TRequestModel : struct
    {
        void Execute(TRequestModel request);
    }
    public interface UseCase
    {
        void Execute();
    }
}
