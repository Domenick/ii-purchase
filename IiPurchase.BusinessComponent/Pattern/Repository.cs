﻿using System.Collections.Generic;

namespace IiPurchase.BusinessComponent.Pattern
{
    public interface Repository<TBusinessObject>
        where TBusinessObject : class
    {
        void Add(TBusinessObject value);
        void Delete(TBusinessObject value);
        IEnumerable<TBusinessObject> GetAll();
        TBusinessObject Get(int id);
        int GetId(TBusinessObject businessObject);
    }
}
