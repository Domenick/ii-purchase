﻿using System;

namespace IiPurchase.BusinessComponent.Pattern
{
    public interface BusinessTransaction : IDisposable
    {
        Repository<Model.Product> Products { get; }
        void Save();
    }
}
