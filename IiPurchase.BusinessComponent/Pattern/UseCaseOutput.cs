﻿namespace IiPurchase.BusinessComponent.Pattern
{
    public interface UseCaseOutput<TResponseModel> where TResponseModel : struct
    {
        void Emit(TResponseModel response);
    }
    public interface UseCaseOutput
    {
        void Emit();
    }
}
