﻿using IiPurchase.BusinessComponent.Pattern;
using System;
using System.Linq;

namespace IiPurchase.BusinessComponent.UseCases
{
    public class RetrieveProductList : Pattern.UseCase
    {
        private UseCaseOutput<ResponseModel> output_;
        private BusinessTransaction transaction_;
        public struct ResponseModel
        {
            public (int Id, string Name, decimal Price, int Amount)[] Products { get; set; }
            public override bool Equals(object second)
            {
                if (second == null || this.GetType() != second.GetType())
                {
                    return false;
                }
                var secondModel = (ResponseModel)second;
                if (this.Products == null && secondModel.Products == null)
                {
                    return true;
                }
                return this.Products.SequenceEqual(secondModel.Products);
            }
            public override int GetHashCode()
            {
                return this.Products?.Aggregate(0, (h, p) => h ^ p.GetHashCode()) ?? 0;
            }
        }
        public RetrieveProductList(Pattern.UseCaseOutput<ResponseModel> output,
            Pattern.BusinessTransaction transaction)
        {
            if (output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(output));
            }
            this.output_ = output;
            this.transaction_ = transaction;
        }
        public void Execute()
        {
            this.output_.Emit(new ResponseModel
            {
                Products = this.transaction_.Products.GetAll().Select(
                    p => (this.transaction_.Products.GetId(p), p.Name, p.Price, p.Amount))
                    .ToArray()
            });
        }
    }
}
