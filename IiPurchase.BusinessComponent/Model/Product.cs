﻿using System;

namespace IiPurchase.BusinessComponent.Model
{
    public class Product
    {
        public string Name { get; }
        public decimal Price { get; }
        public int Amount { get; }
        public Product(string name, decimal price, int amount)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }
            if (name == "")
            {
                throw new ArgumentException("Наименование товара не может быть пусто строкой", nameof(name));
            }
            if (price <= 0M)
            {
                throw new ArgumentException("Цена товара должна быть строго положительноц", nameof(price));
            }
            if (amount < 0)
            {
                throw new ArgumentException("Число доступных для продажи единиц товара должно быть неотрицательно");
            }

            this.Name = name;
            this.Price = price;
            this.Amount = amount;
        }
    }
}
