﻿using System;

namespace IiPurchase.BusinessComponent.Model
{
    public abstract class BusinessObject
    {
        public int Id { get; }
        public BusinessObject()
        {
            this.Id = -1;
        }
        public BusinessObject(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Invalid value of id", nameof(id));
            }
            this.Id = id;
        }
    }
}
