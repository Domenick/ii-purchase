﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IiPurchase.BusinessComponent.Model;
using System;

namespace IiPurchase.Tests.BusinessComponent.Model
{
    [TestClass]
    public class ProductTestSuite
    {
        [TestMethod]
        public void ProductCreatedWithoutException()
        {
            new Product("Test Product", 100.5M, 10);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ConstructionFailesIfNameIsNullOrEmpty()
        {
            new Product(null, 100.5M, 10);
            new Product("", 100.5M, 10);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ConstructionFailesIfPriceIsNegativeOrZero()
        {
            new Product("Test Product", -100.5M, 10);
            new Product("Test Product", 0M, 10);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ConstructionFailesIfAmountIsNegative()
        {
            new Product("Test Product", 100.5M, -10);
        }

        [TestMethod]
        public void AfterConstructionNamePropertyReturnsSpecifiedValue()
        {
            string name = "Test Product";
            var product = new Product(name, 100.5M, 10);
            Assert.AreEqual(name, product.Name);
        }

        [TestMethod]
        public void AfterConstructionPricePropertyReturnsSpecifiedValue()
        {
            decimal price = 100.5M;
            var product = new Product("Test Product", price, 10);
            Assert.AreEqual(price, product.Price);
        }

        [TestMethod]
        public void AfterConstructionAmountPropertyReturnsSpecifiedValue()
        {
            int amount = 10;
            var product = new Product("Test Product", 100.5M, amount);
            Assert.AreEqual(amount, product.Amount);
        }
    }
}
