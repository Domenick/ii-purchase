﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IiPurchase.BusinessComponent.Pattern;
using IiPurchase.BusinessComponent.UseCases;
using IiPurchase.BusinessComponent.Model;
using System;
using System.Linq;
using IiPurchase.Tests.BusinessComponent.Mocks;

namespace IiPurchase.Tests.BusinessComponent.UseCases
{
    [TestClass]
    public class RetrieveProductListTestSuite
    {
        private Product[] products_ = new Product[]
        {
            new Product("Test Product 1", 100.5M, 10),
            new Product("Test Product 2", 10.125M, 0),
            new Product("Test Product 3", 850M, 14)
        };
        private RetrieveProductListOutput output_ = new RetrieveProductListOutput();
        private TestBusinessTransaction transaction_ = new TestBusinessTransaction();
        public RetrieveProductListTestSuite()
        {
            this.transaction_.Products.Add(this.products_[0]);
            this.transaction_.Products.Add(this.products_[1]);
            this.transaction_.Products.Add(this.products_[2]);
            this.transaction_.Save();
        }

        [TestMethod]
        public void UseCaseCreatedWithoutException()
        {
            new RetrieveProductList(this.output_, this.transaction_);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void ConstructionFailedIfOutputIsNull()
        {
            new RetrieveProductList(null, this.transaction_);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void ConstructionFailedIfBusinessTransactionIsNull()
        {
            new RetrieveProductList(this.output_, null);
        }

        [TestMethod]
        public void ExecuteNotThrowException()
        {
            new RetrieveProductList(this.output_, this.transaction_).Execute();
        }

        [TestMethod]
        public void ProductListIsSetAfterExecute()
        {
            new RetrieveProductList(this.output_, this.transaction_).Execute();
            var response = this.output_.LastRecievedResponse;
            Assert.IsNotNull(response, "Response is not set");
            var reference = new RetrieveProductList.ResponseModel
            {
                Products = new(int, string, decimal, int)[]
                {
                    (0, this.products_[0].Name, this.products_[0].Price, this.products_[0].Amount),
                    (1, this.products_[1].Name, this.products_[1].Price, this.products_[1].Amount),
                    (2, this.products_[2].Name, this.products_[2].Price, this.products_[2].Amount)
                }
            };
            Assert.AreEqual(reference, response.Value);
        }
    }
}
