﻿using System.Collections.Generic;
using System.Linq;
using IiPurchase.BusinessComponent.Model;
using IiPurchase.BusinessComponent.Pattern;

namespace IiPurchase.Tests.BusinessComponent.Mocks
{
    public class TestBusinessTransaction : BusinessTransaction
    {
        private List<Product> storedProducts_ = new List<Product>();
        private List<Product> currentProducts_ = new List<Product>();
        public Repository<Product> Products { get; }
        public TestBusinessTransaction()
        {
            this.Products = new TestProductRepository(this.currentProducts_);
        }
        
        public void Save()
        {
            this.storedProducts_ = this.currentProducts_.ToList();
        }

        public void Dispose()
        {

        }
    }
}
