﻿using IiPurchase.BusinessComponent.Pattern;
using IiPurchase.BusinessComponent.UseCases;

namespace IiPurchase.Tests.BusinessComponent.Mocks
{
    public class RetrieveProductListOutput : UseCaseOutput<RetrieveProductList.ResponseModel>
    {
        public RetrieveProductList.ResponseModel? LastRecievedResponse { get; private set; }
        public void Emit(RetrieveProductList.ResponseModel response)
        {
            this.LastRecievedResponse = response;
        }
    }
}
