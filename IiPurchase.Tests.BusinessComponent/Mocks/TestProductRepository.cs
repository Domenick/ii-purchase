﻿using System.Collections.Generic;
using System.Linq;
using IiPurchase.BusinessComponent.Pattern;
using IiPurchase.BusinessComponent.Model;
namespace IiPurchase.Tests.BusinessComponent.Mocks
{
    public class TestProductRepository : Repository<Product>
    {
        private List<Product> products_;
        public TestProductRepository(List<Product> products)
        {
            this.products_ = products.ToList();
        }
        public void Add(Product value)
        {
            this.products_.Add(value);
        }

        public void Delete(Product value)
        {
            this.products_.Remove(value);
        }

        public Product Get(int id)
        {
            return this.products_[id];
        }

        public IEnumerable<Product> GetAll()
        {
            return this.products_;
        }

        public int GetId(Product businessObject)
        {
            return this.products_.IndexOf(businessObject);
        }
    }
}
